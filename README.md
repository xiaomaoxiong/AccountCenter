#AccountCenter

标签（空格分隔）： 未分类

---

以Shiro+SpringMVC为客户端技术，对接CAS服务端。

##1. 服务端
CAS服务端通过[simple-cas4-overlay-template](https://github.com/solarisy/simple-cas4-overlay-template)项目打包。证书的生成和配置网上有较多的文章，可以参考，这里不再啰嗦。

CAS中需要配置客户端，否则客户端没有权限接入。如何配置：在webapps/cas/WEB-INF/classes/services目录下新增文件：AccountCenter-10000003.json，其内容如下：
```json
{
  "@class" : "org.jasig.cas.services.RegexRegisteredService",
  "serviceId" : "http://192.168.6.141:8080/AccountCenter/.*",
  "name" : "AccountCenter",
  "id" : 10000003,
  "description" : "AccountCenter App",
  "usernameAttributeProvider" : {
    "@class" : "org.jasig.cas.services.PrincipalAttributeRegisteredServiceUsernameProvider",
    "usernameAttribute" : "cn"
  }
}
```




